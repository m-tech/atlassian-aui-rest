package com.machiel.atlauirest.view.function;

import com.lyncode.jtwig.functions.JtwigFunction;
import com.lyncode.jtwig.functions.annotations.JtwigFunctionDeclaration;
import com.lyncode.jtwig.functions.exceptions.FunctionException;
import com.machiel.atlauirest.exception.UnableToRetrieveSupportSiteMessageException;
import com.machiel.atlauirest.retriever.SupportMessageRetriever;

@JtwigFunctionDeclaration(name = "support_message")
public class SupportMessage implements JtwigFunction {

    @Override
    public Object execute(Object... objects) throws FunctionException {

        SupportMessageRetriever retriever = new SupportMessageRetriever();

        try {
            return retriever.retrieve( (String) objects[0]);
        } catch (UnableToRetrieveSupportSiteMessageException e) {
            return "";
        }

    }
}
