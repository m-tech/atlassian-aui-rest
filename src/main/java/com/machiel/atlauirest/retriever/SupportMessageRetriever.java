package com.machiel.atlauirest.retriever;

import com.machiel.atlauirest.exception.UnableToRetrieveSupportSiteMessageException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;

public class SupportMessageRetriever
{

    public String retrieve(String url) throws UnableToRetrieveSupportSiteMessageException {

        CloseableHttpClient httpClient = HttpClients.createDefault();

        String responseBody = "";

        try {
            HttpGet httpGet = new HttpGet(url);

            String jsonResponse = httpClient.execute(httpGet, new ResponseHandler());

            JSONObject jsonValue = (JSONObject) JSONValue.parse(jsonResponse);

            responseBody = (String) jsonValue.get("message");
        } catch(Exception e) {
            throw new UnableToRetrieveSupportSiteMessageException();
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        return responseBody;
    }

    class ResponseHandler implements org.apache.http.client.ResponseHandler<String> {

        @Override
        public String handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {

            int status = httpResponse.getStatusLine().getStatusCode();

            if (status == 200) {
                HttpEntity entity = httpResponse.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                throw new ClientProtocolException("Unexpected response status: " + status);
            }

        }
    }

}
