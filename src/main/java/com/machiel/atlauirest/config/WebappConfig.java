package com.machiel.atlauirest.config;

import com.lyncode.jtwig.mvc.JtwigViewResolver;
import com.machiel.atlauirest.view.function.SupportMessage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebappConfig extends WebMvcConfigurerAdapter {

    @Bean
    public ViewResolver viewResolver() {
        JtwigViewResolver jtwigViewResolver = new JtwigViewResolver();
        jtwigViewResolver.setPrefix("/WEB-INF/views/");
        jtwigViewResolver.setSuffix(".twig");
        jtwigViewResolver.addFunctions(SupportMessage.class);
        return jtwigViewResolver;
    }

}
